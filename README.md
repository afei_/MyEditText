### 一、效果展示
![image1][1]
![image2][2]

### 二、功能特点
1. 自带清除功能的 ClearEditText
2. 自带下拉功能的 DropEditText

### 三、详细说明
参考这两篇博客：

[ClearEditText，自带清除功能的EditText][3]

[DropEditText，带下拉功能的EditText][4]

[1]: https://img-blog.csdn.net/20160528151547176?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQv/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center
[2]: https://img-blog.csdn.net/20160528170113256?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQv/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center
[3]: https://blog.csdn.net/afei__/article/details/51525019
[4]: https://blog.csdn.net/afei__/article/details/51525574
