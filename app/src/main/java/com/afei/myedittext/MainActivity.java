package com.afei.myedittext;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class MainActivity extends AppCompatActivity {

    private DropEditText dropEditText;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        dropEditText = (DropEditText) findViewById(R.id.drop_edit_text);
        String[] strings = new String[10];
        for (int i = 0; i < 10; i++) {
            strings[i] = "美女" + i + "号";
        }
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, strings);
        dropEditText.setAdapter(adapter);
    }
}